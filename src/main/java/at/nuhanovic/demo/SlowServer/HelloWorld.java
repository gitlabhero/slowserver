package at.nuhanovic.demo.SlowServer;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.*;
import org.eclipse.jetty.util.thread.QueuedThreadPool;


public class HelloWorld extends HttpServlet {
	private static final long serialVersionUID = -3043828122010838408L;
	
	public String instance;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
		
		resp.setContentType("text/html");

		PrintWriter out = resp.getWriter();
		out.println("<html><head><title>Sloooow server</title></head><body>");
		out.println("<h2> Welcome to WIP17 !</h2>");
        out.println("<h2>Hello from the slooooow server " + instance + " :) </h2>");
        out.print("<p>Simulate some Work...");
        out.flush();
        resp.flushBuffer();
        try {
        	// simulate a workload of 400mseks
			Thread.sleep(400);
		} catch (InterruptedException e) {
			out.println("interrupted!</p>");
			return;
		}
        out.println("<strong>done!</strong></p>");
        out.println("</body>");
    }

    public static void main(String[] args) throws Exception{
    	int port = Integer.parseInt(args[0]);
    	Server server = new Server(port);
    	HelloWorld hello = new HelloWorld();
        QueuedThreadPool threadPool = new QueuedThreadPool(3);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        hello.instance = args[1];
    	
        
        context.setContextPath("/");
        server.setHandler(context);
        server.setThreadPool(threadPool);
        context.addServlet(new ServletHolder(hello),"/*");
        server.start();
        server.join();   
    }
}
